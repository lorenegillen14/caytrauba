# Cây Trầu Bà

Cây Trầu Bà Thông tin toàn tập về cây trầu bà như tác dụng trong phong thủy, cách trồng, cách chăm sóc

- Địa chỉ: Số 20 Khu Biệt Thự Ngân Long, Đường Nguyễn Hữu Thọ, X. Phước Kiển, H. Nhà Bè, TP. HCM

- SDT: 19002187

Cây trầu bà hiện nay đang được trồng rất phổ biến ở Việt Nam trong khoảng trường bay đến khách sạn, văn phòng bạn đều thấy cái cây này. Vì dễ sống, dễ săn sóc lại thanh lọc ko khí nên được chọn lọc phổ quát. Mang đa dạng người kể đùa rằng Cây trầu bà là cây dành cho người chây lười, hay là cây trường sinh nếu như đặt đúng chỗ thì sắp như thường cần coi ngó mà nó vẫn sống và vững mạnh.

Cây có lá to màu xanh thuộc hành Mộc nên thích hợp làm cây phong thủy cho người mệnh Mộc, Hỏa mang đến may mắn, thành công và bình an cho gia chủ

https://caytrauba.com/

https://www.deviantart.com/caytrauba

https://www.flickr.com/people/197300520@N05/

https://www.twitch.tv/caytrauba/about
